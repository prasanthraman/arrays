function reduce(elements,cb,startingValue){  
  
    if (typeof (elements) != 'object' || !Array.isArray(elements)) {
        return 0 //datatype no valid
    }
    let index=0
    let accumulator
    if(typeof(startingValue)=='undefined'){
        startingValue=elements[0]
        index=1
    }
    for(;index<elements.length;index++){
        accumulator=cb(startingValue,elements[index],index,elements)
        startingValue=accumulator
    }
return accumulator
}
module.exports=reduce;
