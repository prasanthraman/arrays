function each(elements, cb) {
    if (typeof (elements) != 'object' || !Array.isArray(elements)) {
        return 0 //datatype no valid
    }


    for (let index = 0; index < elements.length; index++) {
        cb(elements[index], index) // calls call back function passing each element and its index from array
    }
}
module.exports = each;