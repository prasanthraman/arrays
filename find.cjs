function find(elements, cb) {
    let conditionSatisfied=false
    if (typeof (elements) != 'object' || !Array.isArray(elements)) {
        return 0 //datatype no valid
    }
    for(index=0;index<elements.length;index++){
        conditionSatisfied=cb(elements[index])
        if(conditionSatisfied){
            return elements[index]
        }
    }
    return undefined
}
module.exports=find;