function flatten(elements,depth=1) {
    if (typeof (elements) != 'object' || !Array.isArray(elements)) {
        return 0 //datatype no valid
    }
    let flatArray=[]
        for(const index in elements){
           flatArray=flatArray.concat(elements[index])
        }
        depth--
        if(depth>0){
            return flatten(flatArray,depth)
        }else{
            
            return flatArray
        }
    
}

module.exports=flatten;

