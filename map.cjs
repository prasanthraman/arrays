function map(elements, cb) {

    if (typeof (elements) != 'object' || !Array.isArray(elements)) {
        return 0 //datatype no valid
    }
    let mappedArray=[]
    for(let index=0; index<elements.length;index++){
        let mappedValue= cb(elements[index],index,elements)
        mappedArray.push(mappedValue)
    }
    return mappedArray
}
module.exports=map;