function filter(elements, cb) {
    let filteredArray=[]
    let conditionSatisfied=false
    if (typeof (elements) != 'object' || !Array.isArray(elements)) {
        return 0 //datatype no valid
    }
    for(let index=0;index<elements.length;index++){
        conditionSatisfied=cb(elements[index],index,elements)
        if(conditionSatisfied===true){
             filteredArray.push(elements[index])
        }
    }
    return filteredArray
}
module.exports=filter;